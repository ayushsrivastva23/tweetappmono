import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'tweet-ui';

  constructor(private router: Router) {

  }

  ngOnInit(): void {
    this.getToken();
  }

  getToken() {
    if (localStorage.getItem('token') == null) {
      this.router.navigate(["login"])
    } else {
      this.router.navigate(["home"])
    }
  }
}


export const appHost = "http://localhost:8080/api/v1.0/tweets"
