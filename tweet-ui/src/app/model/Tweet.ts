import { Timestamp } from "rxjs";

export class Tweet {
  id: number;
  userName: string;
  user: string;
  timestamp: Date;
  body: string;
  likes: number;

  constructor(id, userName, user, timestamp, body, likes) {
    this.userName = userName;
    this.id = id;
    this.user = user;
    this.timestamp = timestamp;
    this.body = body;
    this.likes = likes;
  }
}
