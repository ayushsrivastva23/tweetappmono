export class User {

  firstName: string;
  lastName: string;
  gender: string;
  dateOfBirth: String;
  email: string;
  password: string;
  isActive: boolean;

}
