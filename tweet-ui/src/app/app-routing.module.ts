import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';

const loginPath = "login"
const signUpPath = "register"
const home = "home"

const routes: Routes = [{
  path: loginPath, component: LoginComponent
},
{
  path: signUpPath, component: RegisterComponent
},
{
  path: home, component: HomeComponent
},
{
  path: "forgotPassword", component: ForgotPasswordComponent
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
