import { Component, HostListener, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  timeToEnd = 1000 * 60 * 30;

  constructor(private router: Router) {
    setTimeout(this.logout, this.timeToEnd)
  }

  ngOnInit(): void {
    if (localStorage.getItem("token") == null) {
      this.router.navigate(["login"]);
      return;
    }
  }

  logout(): void {
    if (localStorage.getItem("token") != null) {
      localStorage.removeItem("token");
    }
  }

}
