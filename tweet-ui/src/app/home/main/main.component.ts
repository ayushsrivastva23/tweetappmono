import { HttpClient, HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { appHost } from 'src/app/app.component';
import { Tweet } from 'src/app/model/Tweet';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {


  tweetIsEmpty: boolean;
  tweetBody: Tweet;
  errorMessage: string = "";

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
  }

  postTweet(tweet: NgForm) {

    this.tweetIsEmpty = tweet.controls['postBody'].invalid;

    if (this.tweetIsEmpty) return;


    let params = new HttpParams().set("token", localStorage.getItem("token"))
    this.tweetBody = tweet.controls['postBody'].value;

    this.http.post<Response>(appHost + "/post", this.tweetBody, { params: params }).subscribe((response) => {
      if (response.status !== 200) {
        this.errorMessage = "something went wrong please try again later";
      }
    },
      (error) => {
        this.errorMessage = error.message;
      });



  }

}
