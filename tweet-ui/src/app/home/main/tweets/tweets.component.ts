import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Tweet } from 'src/app/model/Tweet';

@Component({
  selector: 'app-tweets',
  templateUrl: './tweets.component.html',
  styleUrls: ['./tweets.component.css']
})
export class TweetsComponent implements OnInit {

  tweets: Tweet[] = []

  constructor(private http: HttpClient) { }

  ngOnInit(): void {

    this.tweets.push(new Tweet(1, "Ayush Srivastava", "ayush@gmail.com", new Date(), "Wow what a beautiful world!", 250));
    this.tweets.push(new Tweet(1, "Ayush Srivastava", "ayush@gmail.com", new Date(), "Wow what a beautiful world!", 250));
    this.tweets.push(new Tweet(1, "Ayush Srivastava", "ayush@gmail.com", new Date(), "Wow what a beautiful world!", 250));
    this.tweets.push(new Tweet(1, "Ayush Srivastava", "ayush@gmail.com", new Date(), "Wow what a beautiful world!", 250));
    this.tweets.push(new Tweet(1, "Ayush Srivastava", "ayush@gmail.com", new Date(), "Wow what a beautiful world!", 250));

  }

}
