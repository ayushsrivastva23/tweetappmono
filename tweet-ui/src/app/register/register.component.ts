import { DatePipe } from '@angular/common';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Component, Injectable, OnInit } from '@angular/core';
import { FormGroup, NgForm, NgModel } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { isVariableDeclarationList } from 'typescript';
import { Response } from '../model/Response';
import { User } from '../model/User';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
@Injectable()
export class RegisterComponent implements OnInit {
  dateOfBirth: Date;
  invalidForm: boolean;
  success: boolean;
  errorMessage: string = "";
  user: User;
  title = "choose gender";
  response: Response;
  constructor(private http: HttpClient, private router: Router) {
  }

  ngOnInit(): void {
    if (localStorage.getItem("token") !== null) {
      this.router.navigate(["home"]);
      return;
    }
  }

  form: FormGroup;
  onSubmit(registrationForm: NgForm) {
    if (registrationForm.invalid) {
      this.invalidForm = true;
      registrationForm.reset();
      return;
    }
    ;
    this.user = registrationForm.control.value;
    if (this.user.password !== registrationForm.controls['confirmPassword'].value) {
      return;
    }
    this.user.dateOfBirth = this.reformatDate(this.user.dateOfBirth);
    this.http.post<any>("http://localhost:8080/auth/save", this.user).subscribe(dataResponse => {
      this.response = dataResponse;
      if (this.response != undefined && this.response.status == 200) {
        this.invalidForm = false;
        this.success = true;
        this.errorMessage = "";
        localStorage.setItem("token", this.user.email)
        this.router.navigate(["home"])
      } else {
        this.errorMessage = this.response.message;
      }
    }, (error) => {
      this.response = error;
      this.invalidForm = true;
      this.success = false;
      this.errorMessage = this.response.message;
    });


  }
  reformatDate(dateOfBirth: String): String {
    console.log(dateOfBirth);
    var year = dateOfBirth.substring(0, 4);
    var month = dateOfBirth.substring(5, 7);
    var day = dateOfBirth.substring(8, 10);
    return day + "/" + month + "/" + year;
  }
}


