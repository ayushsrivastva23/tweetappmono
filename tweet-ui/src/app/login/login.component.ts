
import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, HostListener, Injectable, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { appHost } from '../app.component';
import { Response } from '../model/Response';
import { User } from '../model/User';
import { UserRequest } from '../model/UserRequest';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],

})
@Injectable()
export class LoginComponent implements OnInit {

  loginFormIsInvalid: boolean;
  userInfo: UserRequest;
  user: User;
  response: Response;
  errorMessage: string;
  constructor(private elementRef: ElementRef, private http: HttpClient, private router: ActivatedRoute, private routerNav: Router) {

  }

  ngOnInit(): void {
    if (localStorage.getItem("token") !== null) {
      this.routerNav.navigate(["home"]);
      return;
    }
  }

  onSubmit(form: NgForm) {
    this.errorMessage = undefined;
    if (form.invalid) {
      this.loginFormIsInvalid = true;
      return;
    }
    this.userInfo = form.control.value;
    this.http.post<any>(appHost + "/auth/login", this.userInfo).subscribe((response) => {
      this.loginFormIsInvalid = false;
      this.response = response;
      if (this.response.status !== 200) {
        this.errorMessage = this.response.message;
      } else {
        this.routerNav.navigate(["home"]);
        localStorage.setItem('token', this.userInfo.email);
      }
    }, (error) => {
      this.errorMessage = error.message;
    });
  }
}
