import { HttpClient, HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { emit } from 'process';
import { Subscription } from 'rxjs';
import { appHost } from '../app.component';
import { Response } from '../model/Response';
import { User } from '../model/User';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  userIdIsInValid: boolean = true;
  thereIsSomeError: boolean = false;
  errorMessage: string = "";
  successMessage = "user verified!"
  passwordChangedSuccess = [false, "Password changed!"]
  oldPassword: string;

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
  }

  onSubmit(form: NgForm) {

    let email = form.controls['email'].value;
    let oldpassword = form.controls['oldpassword'].value;
    let newpassword = form.controls['newpassword'];

    if (this.verifyOldPassword(oldpassword)) {
      if (newpassword.invalid) {
        this.thereIsSomeError = true;
        this.errorMessage = "Invalid new password!";
        return;
      }
      let params = new HttpParams().set("email", email).set("password", newpassword.value)
      this.http.get<boolean>(appHost + "/auth/changePassword", { params: params }).subscribe((response) => {

        this.passwordChangedSuccess[0] = response;
        if (!this.passwordChangedSuccess[0]) {
          this.thereIsSomeError = true;
          this.errorMessage = "Update Failed!";
        }

      }, (error) => {
        this.thereIsSomeError = true;
        this.errorMessage = error.message;
      })
    } else {
      this.thereIsSomeError = true;
      this.errorMessage = "Old password is incorrect";
      return;
    }

  }

  //when verifying email store old password and check with new password later on

  verifyOldPassword(oldPassword: string): boolean {
    //let params = new HttpParams().set("password", oldpassword).set("email", email)
    /*this.http.get<boolean>(appHost + "/auth/verifyOldPassword", { params: params }).subscribe((repsonse) => {
      oldPasswordIsCorrect = repsonse;
      return repsonse;
    },
      (error) => {
        this.thereIsSomeError = true;
        this.errorMessage = error.message;
        return false;
      });*/
    return this.oldPassword === oldPassword;
  }

  userEmail: string;
  verifyUser(email) {
    this.thereIsSomeError = false;
    this.userEmail = email.control.value;

    if (email.value === undefined || email.value.length === 0) {
      this.thereIsSomeError = true;
      this.errorMessage = "Email is empty";
      return;
    }

    let params = new HttpParams().set("email", this.userEmail)
    this.http.get<User>(appHost + "/auth/verify", { params: params }).subscribe((response) => {
      if (!response) {
        this.userIdIsInValid = true;
        this.thereIsSomeError = true;
        this.errorMessage = "User does not exist!"
      } else {
        this.userIdIsInValid = false;
        this.oldPassword = response.password;
      }

      return response;
    },
      (error) => {
        this.thereIsSomeError = true;
        this.errorMessage = error.message;
      });
  }

}


