package com.tweetapp.dao;

import com.tweetapp.model.entity.Tweet;

import java.util.List;

public interface TweetDao {
    public List<Tweet> loadAllTweets();
    public Tweet postTweet(Tweet tweet);
}
