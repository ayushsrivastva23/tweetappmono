package com.tweetapp.dao.impl;

import com.tweetapp.dao.TweetDao;
import com.tweetapp.model.entity.Tweet;
import com.tweetapp.repository.TweetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TweetDaoImpl implements TweetDao {

    @Autowired
    TweetRepository tweetRepository;

    @Override
    public List<Tweet> loadAllTweets() {
        //sort tweets based on timestamp in desc order
        return tweetRepository.findAll().stream().sorted((t1, t2) -> {
            return t2.getTimestamp().compareTo(t1.getTimestamp());
        }).collect(Collectors.toList());

    }

    @Override
    public Tweet postTweet(Tweet tweet) {
        return tweetRepository.save(tweet);
    }
}
