package com.tweetapp.dao.impl;

import com.mongodb.client.result.UpdateResult;
import com.tweetapp.dao.UserDao;
import com.tweetapp.domain.response.TweetAppResponse;
import com.tweetapp.exception.InvalidUserDetailsException;
import com.tweetapp.exception.UserAlreadyExistExcpetion;
import com.tweetapp.exception.UserNotFoundException;
import com.tweetapp.exception.util.ExceptionMessage;
import com.tweetapp.model.UserRequest;
import com.tweetapp.model.entity.User;
import com.tweetapp.repository.UserRepository;
import com.tweetapp.service.GenerateNextSequence;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserDaoImpl implements UserDao {

    public static final Integer USER_KEY = 1;

    @Autowired
    UserRepository userRepository;

    @Autowired
    GenerateNextSequence generateNextSequence;

    @Autowired
    MongoOperations mongoOperations;

    @Override
    public User getUserDetails(Integer id) {
        Optional<User> user = userRepository.findById(id);
        if(!user.isPresent()){
            throw new UserNotFoundException("User Not Exist against the given id");
        }
        return user.get();
    }

    @Override
    public User saveUser(User user) {
        User saved;
        user.setId(generateNextSequence.getNextSequence(USER_KEY));
        verifyUserByEmail(user.getEmail());
        if(user.getId()!=0){
            saved = userRepository.save(user);
        }else{
            throw new InvalidUserDetailsException("Invalid Credentials for user");
        }
       return saved;
    }


    @Override
    public List<User> getAllUser() {
        return userRepository.findAll();
    }

    @Override
    public User getUserByEmailAndPassword(UserRequest userRequest) {
        Query query = new Query();
        query.addCriteria(Criteria.where("email").is(userRequest.getEmail()).and("password").is(userRequest.getPassword()));
        User user =  mongoOperations.findOne(query,User.class);
        if(user == null)
            throw new UserNotFoundException(ExceptionMessage.USER_NOT_EXIST);
        updateUserLoginStatus(query,user);
        return user;
    }

    @Override
    public User verifyUserByEmailAndPassword(UserRequest userRequest) {
        Query query = new Query();
        query.addCriteria(Criteria.where("email").is(userRequest.getEmail()).and("password").is(userRequest.getPassword()));
        User user =  mongoOperations.findOne(query,User.class);
        if(user == null)
            throw new UserNotFoundException(ExceptionMessage.USER_NOT_EXIST);
        return user;
    }

    @Override
    public User getUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public boolean changePassword(String email, String password) {
        Query query = new Query();
        query.addCriteria(Criteria.where("email").is(email));
        Update update = new Update();
        update.set("password",password);
        UpdateResult result = mongoOperations.updateFirst(query,update,User.class);
        return  result.getModifiedCount()>0;
    }

    private void updateUserLoginStatus(Query query,User user) {
        user.setIsActive(true);
        Update update = new Update();
        update.set("isActive",true);
        mongoOperations.updateFirst(query,update,User.class);
    }

    private void verifyUserByEmail(String email) {
        Query query = new Query();
        query.addCriteria(Criteria.where("email").is(email));
        User user = mongoOperations.findOne(query,User.class);
        if(user !=null) throw new UserAlreadyExistExcpetion(ExceptionMessage.USER_ALREADY_EXIST);
    }

}
