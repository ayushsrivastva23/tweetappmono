package com.tweetapp.dao;

import com.tweetapp.domain.response.TweetAppResponse;
import com.tweetapp.model.UserRequest;
import com.tweetapp.model.entity.User;

import java.util.List;

public interface UserDao {
    public User getUserDetails(Integer id);
    public User saveUser(User user);
    public List<User> getAllUser();
    public User getUserByEmailAndPassword(UserRequest userRequest);
    public User verifyUserByEmailAndPassword(UserRequest userRequest);
    public User getUserByEmail(String email);
    public boolean changePassword(String email, String password);
}
