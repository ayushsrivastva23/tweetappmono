package com.tweetapp.service;

import com.tweetapp.model.entity.CustomSequences;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import static com.mongodb.client.model.Filters.where;
import static org.springframework.data.mongodb.core.FindAndModifyOptions.options;
import static org.springframework.data.mongodb.core.query.Query.query;

@Service
public class GenerateNextSequence {
    @Autowired
    MongoOperations mongoOperations;

    public int getNextSequence(Integer sequenceName) {
        Query query = new Query(Criteria.where("_id").is(sequenceName));
        CustomSequences counter = mongoOperations.findAndModify(
                query,
                new Update().inc("sequence", 1),
                options().returnNew(true).upsert(true),
                CustomSequences.class);
        return null==counter?1:counter.getSequence();
    }
}
