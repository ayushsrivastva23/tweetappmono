package com.tweetapp.service.impl;

import com.tweetapp.dao.TweetDao;
import com.tweetapp.domain.request.AddTweetRequest;
import com.tweetapp.exception.TweetEmptyError;
import com.tweetapp.exception.util.ExceptionMessage;
import com.tweetapp.model.entity.Tweet;
import com.tweetapp.service.GenerateNextSequence;
import com.tweetapp.service.TweetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class TweetServiceImpl implements TweetService {

    @Autowired
    TweetDao tweetDao;

    @Autowired
    GenerateNextSequence generateNextSequence;

    @Override
    public List<Tweet> loadAllTweets() {
        //TODO:implement dao for loading all tweets
        return tweetDao.loadAllTweets();
    }

    @Override
    public Tweet postTweet(AddTweetRequest tweetRequest) {
        Tweet tweet = Tweet.builder()
                .body(tweetRequest.getBody())
                .id(generateNextSequence.getNextSequence(0))
                .likes(0)
                .timestamp(new Date())
                .title(tweetRequest.getTitle())
                .userEmail(tweetRequest.getUserEmail())
                .build();
        return tweetDao.postTweet(tweet);
    }
}
