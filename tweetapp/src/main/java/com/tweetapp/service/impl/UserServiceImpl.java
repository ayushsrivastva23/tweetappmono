package com.tweetapp.service.impl;

import com.tweetapp.dao.UserDao;
import com.tweetapp.domain.response.TweetAppResponse;
import com.tweetapp.exception.EmptyUsersListException;
import com.tweetapp.exception.UserNotFoundException;
import com.tweetapp.exception.UserNotSavedException;
import com.tweetapp.exception.util.ExceptionMessage;
import com.tweetapp.model.UserRequest;
import com.tweetapp.model.entity.User;
import com.tweetapp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserDao userDao;

    @Override
    public User getUserById(Integer id) {
        return userDao.getUserDetails(id);
    }

    @Override
    public User saveUser(User user) {
        User saved = userDao.saveUser(user);
        if(saved == null){
            throw new UserNotSavedException("User not saved due to some error");
        }
        return user;
    }

    @Override
    public List<User> getAllUser() {
        List<User> users = userDao.getAllUser();
        if(!users.isEmpty())
            return users;
        throw new EmptyUsersListException(ExceptionMessage.USER_NOT_FOUND);
    }

    @Override
    public User verifyUserByEmailAndPassword(UserRequest userRequest) {
        User user = userDao.getUserByEmailAndPassword(userRequest);
        if(user==null)
            throw new UserNotFoundException(ExceptionMessage.USER_NOT_EXIST);
        return user;
    }

    @Override
    public User doesUserExist(String email) {
        return userDao.getUserByEmail(email);
    }

    @Override
    public User verifyOldPassword(String email, String password) {
        UserRequest userRequest = new UserRequest();
        userRequest.setEmail(email);
        userRequest.setPassword(password);
        return userDao.verifyUserByEmailAndPassword(userRequest);
    }

    @Override
    public boolean changePassword(String email, String password) {
        return userDao.changePassword(email,password);
    }
}
