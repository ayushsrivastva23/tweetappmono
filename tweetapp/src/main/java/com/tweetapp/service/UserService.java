package com.tweetapp.service;

import com.tweetapp.domain.response.TweetAppResponse;
import com.tweetapp.model.UserRequest;
import com.tweetapp.model.entity.User;

import java.util.List;

public interface UserService {
    public User getUserById(Integer id);
    public User saveUser(User user);
    public List<User> getAllUser();
    public User verifyUserByEmailAndPassword(UserRequest userRequest);
    public User doesUserExist(String email);
    public User verifyOldPassword(String email,String password);
    public boolean changePassword(String email, String password);
}
