package com.tweetapp.service;

import com.tweetapp.domain.request.AddTweetRequest;
import com.tweetapp.model.entity.Tweet;

import java.util.List;

public interface TweetService{
    public List<Tweet> loadAllTweets();
    public Tweet postTweet(AddTweetRequest tweet);
}
