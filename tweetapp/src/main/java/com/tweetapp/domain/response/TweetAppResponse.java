package com.tweetapp.domain.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class TweetAppResponse<T> {
    private Integer status;
    private String  message;
    private Object result;
}
