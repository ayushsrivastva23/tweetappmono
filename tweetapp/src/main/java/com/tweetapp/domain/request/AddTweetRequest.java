package com.tweetapp.domain.request;

import lombok.*;

import java.util.Date;

@Getter
@Setter
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddTweetRequest {
    private String userEmail;
    private String title;
    private String body;
}

