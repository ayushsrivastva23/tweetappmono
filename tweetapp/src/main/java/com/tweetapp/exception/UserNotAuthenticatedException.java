package com.tweetapp.exception;

public class UserNotAuthenticatedException extends UserNotFoundException {
    public UserNotAuthenticatedException(String message) {
        super(message);
    }
}
