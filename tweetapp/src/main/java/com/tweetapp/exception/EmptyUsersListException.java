package com.tweetapp.exception;

public class EmptyUsersListException extends UserNotFoundException {
    public EmptyUsersListException(String no_users_in_db) {
        super(no_users_in_db);
    }
}
