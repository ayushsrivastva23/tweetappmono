package com.tweetapp.exception;

public class TweetEmptyError extends RuntimeException {
    public TweetEmptyError(String message){
        super(message);
    }
}
