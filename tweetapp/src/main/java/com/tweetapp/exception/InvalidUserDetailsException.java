package com.tweetapp.exception;

public class InvalidUserDetailsException extends RuntimeException {
    public InvalidUserDetailsException(String invalid_credentials_for_user) {
        super(invalid_credentials_for_user);
    }
}
