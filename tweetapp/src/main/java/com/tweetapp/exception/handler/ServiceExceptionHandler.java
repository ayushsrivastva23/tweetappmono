package com.tweetapp.exception.handler;

import com.tweetapp.domain.response.TweetAppResponse;
import com.tweetapp.exception.InvalidUserDetailsException;
import com.tweetapp.exception.UserAlreadyExistExcpetion;
import com.tweetapp.exception.UserNotFoundException;
import com.tweetapp.exception.util.ExceptionMessage;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingRequestHeaderException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class ServiceExceptionHandler {

    @ExceptionHandler(value = {UserNotFoundException.class})
    public TweetAppResponse<Object> userNotFoundException(UserNotFoundException exception){
        return new TweetAppResponse<>(HttpStatus.NOT_FOUND.value(),exception.getMessage(),null);
    }

    @ExceptionHandler(value = {InvalidUserDetailsException.class})
    public TweetAppResponse<Object> invalidUserDetailsException(InvalidUserDetailsException exception){
        return new TweetAppResponse<>(HttpStatus.BAD_REQUEST.value(),exception.getMessage(),null);
    }

    @ExceptionHandler(value = {MethodArgumentNotValidException.class})
    public TweetAppResponse<Object> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return new TweetAppResponse<>(HttpStatus.BAD_REQUEST.value(),errors.toString(),null);
    }

    @ExceptionHandler(value = HttpMessageNotReadableException.class)
    public TweetAppResponse<Object> hanldeNotReadableError(HttpMessageNotReadableException exception){
        TweetAppResponse<Object> error =  new TweetAppResponse<>();
        error.setStatus(HttpStatus.BAD_REQUEST.value());
        if(exception.getMessage()!=null) {
            if (exception.getMessage().contains("User$Gender")) {
                error.setMessage(ExceptionMessage.INVALID_GENDER);
            } else if (exception.getMessage().contains("date")){
                error.setMessage(ExceptionMessage.INVALID_DATE);
            }else if(exception.getMessage().contains("body is missing")){
                error.setMessage(ExceptionMessage.TWEET_EMPTY_ERROR);
            }
            else {
                error.setMessage(ExceptionMessage.UNKNOWN_ERROR);
            }
        }
        return error;
    }

    @ExceptionHandler(value = {UserAlreadyExistExcpetion.class})
    public TweetAppResponse<Object> userAlreadyExistException(UserAlreadyExistExcpetion exception){
        return new TweetAppResponse<>(HttpStatus.CONFLICT.value(),exception.getMessage(),null);
    }

    @ExceptionHandler(value = MissingRequestHeaderException.class)
    public String loginTokenMissing(){
        return "login token missing!";
    }
}
