package com.tweetapp.exception.util;

public class ExceptionMessage {
    public static final String USER_NOT_EXIST = "User does not exist!";
    public static final String USER_NOT_FOUND = "No Users found!";
    public static final String INVALID_GENDER = "Please choose correct gender";
    public static final String INVALID_DATE = "Invalid date!";
    public static final String UNKNOWN_ERROR = "Some unknown error occured please try again later";
    public static final String USER_ALREADY_EXIST = "USER ALREADY EXIST";
    public static final String LOGIN_ERROR = "Please login to continue!";
    public static final String TWEET_EMPTY_ERROR = "Empty tweet cannot be posted";

}
