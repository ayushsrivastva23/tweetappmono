package com.tweetapp.model.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import org.bson.types.Binary;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.time.LocalDate;
import java.util.Date;

@Getter
@Setter
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "users")
public class User {
    @Id
    private Integer id;
    @Size(min = 5,max = 10,message = "first name should be 5 to 10 characters")
    private String firstName;
    @Size(min = 5,max = 10,message = "last name should be 5 to 10 characters")
    private String lastName;
    @NotNull
    private Gender gender;
    @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "dd/MM/yyyy")
    private Date dateOfBirth;
    @NotBlank(message = "email cannot be blank")
    @Email(message = "please enter a valid email")
    private String email;
    @NotEmpty(message = "password cannot be empty")
    @Size(min = 6, max = 8, message = "password must be 6 to 8 characters long")
    private String password;

    private Boolean isActive=false;

    private Binary image;

    private enum Gender{MALE,FEMALE,NEUTRAL};
}
