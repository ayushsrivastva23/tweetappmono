package com.tweetapp.model.entity;

import lombok.*;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotBlank;
import java.util.Date;

@Getter
@Setter
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "tweets")
public class Tweet {
    private Integer id;
    private String userEmail;
    private Date timestamp = new Date() ;
    @NotBlank
    private String title;
    @NotBlank
    private String body;

    private Integer likes=0;

}
