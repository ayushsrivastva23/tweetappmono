package com.tweetapp.repository;

import com.tweetapp.model.entity.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends MongoRepository<User,Integer> {
    public User findByEmail(String email);
}
