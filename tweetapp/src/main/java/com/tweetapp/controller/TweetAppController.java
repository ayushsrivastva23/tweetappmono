package com.tweetapp.controller;

import com.tweetapp.domain.request.AddTweetRequest;
import com.tweetapp.domain.response.TweetAppResponse;
import com.tweetapp.exception.TweetEmptyError;
import com.tweetapp.exception.UserNotAuthenticatedException;
import com.tweetapp.exception.util.ExceptionMessage;
import com.tweetapp.model.entity.Tweet;
import com.tweetapp.service.TweetService;
import com.tweetapp.utils.auth.Auth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
public class TweetAppController {

    @Autowired
    TweetService tweetService;


   @PostMapping("/add")
    public TweetAppResponse<Tweet> postTweet(@RequestHeader("auth-token") String token,@Valid @RequestBody AddTweetRequest tweet){
        Auth.isUserLoggedIn(token);
        return new TweetAppResponse<Tweet>(HttpStatus.OK.value(), "success", tweetService.postTweet(tweet));
    }

    @GetMapping("/all")
    public TweetAppResponse<Tweet> loadAllTweets(@RequestHeader("auth-token") String token) {
        Auth.isUserLoggedIn(token);
        List<Tweet> allTweets = tweetService.loadAllTweets();
        return new TweetAppResponse<>(HttpStatus.OK.value(), "success", allTweets);
    }

}
