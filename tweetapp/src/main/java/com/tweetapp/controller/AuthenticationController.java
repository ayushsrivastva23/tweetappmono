package com.tweetapp.controller;

import com.tweetapp.domain.response.TweetAppResponse;
import com.tweetapp.exception.UserNotAuthenticatedException;
import com.tweetapp.exception.util.ExceptionMessage;
import com.tweetapp.model.UserRequest;
import com.tweetapp.model.entity.User;
import com.tweetapp.service.UserService;
import com.tweetapp.utils.Message;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/auth")
@CrossOrigin("*")
public class AuthenticationController {

    @Autowired
    UserService userService;


    @GetMapping("/verify")
    public User doesUserExist(@RequestParam String email) {
        return userService.doesUserExist(email);
    }


    @GetMapping("/changePassword")
    public boolean changePassword(@RequestParam String email,@RequestParam String password) {
        return userService.changePassword(email,password);
    }

    @PostMapping("/login")
    public TweetAppResponse<User> login(@RequestBody UserRequest userRequest) {
        User user = userService.verifyUserByEmailAndPassword(userRequest);
        return new TweetAppResponse<>(HttpStatus.OK.value(), Message.SUCCESS, user);
    }


    @GetMapping("/user")
    public TweetAppResponse<User> getUserDetails(@RequestParam Integer id) {
        return new TweetAppResponse<>(HttpStatus.OK.value(), "success", userService.getUserById(id));
    }

    @PostMapping("/save")
    public TweetAppResponse<User> saveUserDetails(@Valid @RequestBody User user) {
        log.info("User registration service started : {}", user);
        User retreivedUser = userService.saveUser(user);
        log.info("User registration service ended : {}", retreivedUser);
        return new TweetAppResponse<>(HttpStatus.OK.value(), "success", retreivedUser);
    }

    @GetMapping("/getAllUsers")
    public TweetAppResponse<List<User>> getAllUsers(@RequestParam Map<String, String> token) {
        if (token == null) {
            throw new UserNotAuthenticatedException(ExceptionMessage.LOGIN_ERROR);
        }
        List<User> users = userService.getAllUser();
        return new TweetAppResponse<>(HttpStatus.OK.value(), "success", users);
    }

    @GetMapping("/verifyOldPassword")
    public TweetAppResponse<User> verifyOldPassword(@RequestParam String email, @Valid @RequestParam String password) {
        return new TweetAppResponse<>(HttpStatus.OK.value(),"success",userService.verifyOldPassword(email, password));
    }
}
