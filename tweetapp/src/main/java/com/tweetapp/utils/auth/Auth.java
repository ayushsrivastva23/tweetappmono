package com.tweetapp.utils.auth;

import com.tweetapp.exception.UserNotAuthenticatedException;
import com.tweetapp.exception.util.ExceptionMessage;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class Auth {

    public static void isUserLoggedIn(String token){
        if(token==null)
            throw new UserNotAuthenticatedException(ExceptionMessage.LOGIN_ERROR);
    }
}
